//
//  MyViewController.m
//  TestLocalNotification
//
//  Created by Sooyo on 14-12-22.
//  Copyright (c) 2014年 Sooyo. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "AppDelegate.h"
#import "BellRinger.h"
#import "MyViewController.h"


#define rgba(a , b , c , d) [UIColor colorWithRed:(a) / 255.f  green:(b) / 255.f  blue:(c) / 255.f alpha:(d)]
#define rgb(a , b , c)  rgba(a , b , c , 1)

static const NSTimeInterval RestInterval = 5 * 60;      // The next alarm would come after 5 minutes
static const NSInteger AllowMaximumIgnoreTime = 3;     // User ignores the bell for 3 times will aware the backend


/* Class */
@interface MyViewController () <AVCaptureVideoDataOutputSampleBufferDelegate>

@property (weak, nonatomic) IBOutlet UIView *centerContainer;
@property (weak, nonatomic) IBOutlet UIView *bottomContainer;

@property (weak, nonatomic) IBOutlet UILabel *timeTextLabel;
@property (weak, nonatomic) IBOutlet UITextView *noticeTextView;
@property (weak, nonatomic) IBOutlet UILabel *nextAlarmNoticeView;

@property (nonatomic , strong) UIButton *cameraButton;
@property (nonatomic , strong) UIButton *closeButton;
@property (nonatomic , strong) UIButton *okButton;
@property (nonatomic , strong) UIButton *helpButton;

@property (nonatomic , assign) BOOL isCameraOn;
@property (nonatomic , assign) BOOL isAlarming;
@property (nonatomic , assign) NSInteger noResponseTime;

@property (nonatomic , strong) UIView *previewView;
@property (nonatomic , strong) NSString* currentTimeString;
@property (nonatomic , strong) NSDateFormatter *timeFormatter;
@property (nonatomic , strong) NSArray *alarmTimeArray;

@property (nonatomic , strong) NSTimer *updateTimeLabelTimer;
@property (nonatomic , strong) NSTimer *timeOutTimer;

@property (nonatomic , strong) AVCaptureSession *session;
@property (nonatomic , strong) AVCaptureVideoPreviewLayer *sessionPreLayer;

@property (nonatomic , strong) BellRinger *bellRinger;


/* Init the interface programmatically */
- (void)createTopButtons;
- (void)createBottomButtons;

- (void)updateTimeLabel;
- (void)updateNextAlarmTimeLabel;

- (void)onCameraButtonPressed:(id)sender;
- (void)onCloseButtonPressed;
- (void)onOKButtonPressed;
- (void)onHelpButtonPressed:(id)sender;

- (void)startVideoRecording:(BOOL)start;

- (void)waitForNextAlarm;
- (void)autoStartRingTheBell;
- (void)onStartRingTheBell;
- (void)stopRingTheBell;
- (void)scheduleNextAlarm;
- (void)removeOKButtonBackgroundAnimation;

@end

@implementation MyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.isCameraOn = NO;
    self.isAlarming = NO;
    self.noResponseTime = 0;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self createTopButtons];
    [self createBottomButtons];
    [self setLifeLineStatus:LifeLineStatusResting];
    
    self.previewView = [[UIView alloc] init];
    [self.previewView setBackgroundColor:rgb(0 , 0 , 0)];
    
    self.timeFormatter = [[NSDateFormatter alloc] init];
    [self.timeFormatter setDateFormat:@"HH:mm:ss"];
    
    self.alarmTimeArray = @[@"07:00" , @"12:00" , @"17:00"];
    
    [self updateNextAlarmTimeLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoStartRingTheBell) name:KLifeLineQueryNotificationName object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _cameraButton = nil;
    _closeButton = nil;
    _okButton = nil;
    _helpButton = nil;
    
    _session = nil;
    _sessionPreLayer = nil;
    
    _previewView = nil;
    _currentTimeString = nil;
    _timeFormatter = nil;
    
    [_updateTimeLabelTimer invalidate];
    _updateTimeLabelTimer = nil;
    
    [self.timeOutTimer invalidate];
    self.timeOutTimer = nil;
    
    [self.bellRinger stopRingingTheBell];
    self.bellRinger = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.previewView setFrame:self.centerContainer.frame];
    [self.previewView setAlpha:0];
    
    self.updateTimeLabelTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimeLabel) userInfo:nil repeats:YES];
}

- (BOOL)prefersStatusBarHidden
{
    return self.isCameraOn;
}

- (void)setLifeLineStatus:(LifeLineStatus)lifeLineStatus
{
    _lifeLineStatus = lifeLineStatus;
    
    switch (lifeLineStatus) {
        case LifeLineStatusPausingForNextRing:
            [self.noticeTextView setTextAlignment:NSTextAlignmentLeft];
            [self.noticeTextView setText:@"稍后5分钟，我们会再次问候您，请您务必报个平安"];
            break;
            
        case LifeLineStatusResting:
            [self.noticeTextView setTextAlignment:NSTextAlignmentRight];
            [self.noticeTextView setText:@"我们就在您的身边\n\t\t服务热线：22281331"];
            break;
            
        case LifeLineStatusRingingBell:
            [self.noticeTextView setTextAlignment:NSTextAlignmentCenter];
            [self.noticeTextView setText:@"请您给我们报个平安吧"];
            [self onStartRingTheBell];
            break;
            
        default:
            break;
    }
}

- (void)autoStartRingTheBell
{
    [self setLifeLineStatus:LifeLineStatusRingingBell];
}

- (void)onStartRingTheBell
{
    if (self.isAlarming)
        return;
    self.isAlarming = YES;
    
    /* Add an animation to the OK button */
    CABasicAnimation *backgroundColorAnimation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    backgroundColorAnimation.fromValue = (id)[UIColor whiteColor].CGColor;
    backgroundColorAnimation.toValue = (id)rgba(255 , 255 , 255 , 0.7).CGColor;
    backgroundColorAnimation.autoreverses = YES;
    backgroundColorAnimation.repeatCount = 1000.f;
    backgroundColorAnimation.duration = 0.7;
    
    [self.okButton.layer addAnimation:backgroundColorAnimation forKey:@"BackgroundColor"];
    
    /* Ring the bell */
    if (self.bellRinger == nil)
        self.bellRinger = [[BellRinger alloc] init];
    [self.bellRinger startToRingTheBell];
    
    self.timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(waitForNextAlarm) userInfo:nil repeats:NO];
}

- (void)stopRingTheBell
{
    [self.bellRinger stopRingingTheBell];
    
    [self.timeOutTimer invalidate];
    self.timeOutTimer = nil;
    
    self.noResponseTime = 0;
}

- (void)scheduleNextAlarm
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *date = [formatter dateFromString:self.nextAlarmNoticeView.text];
    UILocalNotification *notify = [[UILocalNotification alloc] init];
    
    notify.fireDate = date;
    notify.alertBody = @"平安钟，请您报平安";
    notify.soundName = @"Beepbeep.aif";
    notify.userInfo = @{@"Type" : @"LifeLine"};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notify];
}

- (void)removeOKButtonBackgroundAnimation
{
    [self.okButton.layer removeAnimationForKey:@"BackgroundColor"];
}

- (void)waitForNextAlarm
{
    self.isAlarming = NO;
    [self removeOKButtonBackgroundAnimation];
    
    [self.timeOutTimer invalidate];
    self.timeOutTimer = nil;
    
    [self.bellRinger stopRingingTheBell];
    
    /* Check if we need to inform the management center */
    if (++ self.noResponseTime > AllowMaximumIgnoreTime)
    {
        /* Do transition */
        // ...
        
        return;
    }
    
    /* Update next alarm text */
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *date = [formatter dateFromString:self.timeTextLabel.text];
    NSDate *nextAlarm = [date dateByAddingTimeInterval:RestInterval];
    
    [formatter setDateFormat:@"HH:mm"];
    [self.nextAlarmNoticeView setText:[formatter stringFromDate:nextAlarm]];

    /* Schedule another notification a litte later */
    UILocalNotification *notify = [[UILocalNotification alloc] init];
    NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:RestInterval];
    
    notify.fireDate = fireDate;
    notify.alertBody = @"平安钟，请您报平安";
    notify.soundName = @"Beepbeep.aif";
    notify.userInfo = @{@"Type" : @"LifeLine"};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notify];
    
    /* Set the state to waiting */
    [self setLifeLineStatus:LifeLineStatusPausingForNextRing];
}


- (void)createTopButtons
{
    CGFloat size = 0.5 * CGRectGetWidth(self.view.bounds);

    self.cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cameraButton setFrame:CGRectMake(-1 , -1 , size , size)];
    [self.cameraButton setBackgroundColor:rgb(227 , 66 , 72)];
    [self.cameraButton setTitleColor:rgb(255 , 255 , 255) forState:UIControlStateNormal];
    [self.cameraButton setImageEdgeInsets:UIEdgeInsetsMake(30 , 0 , 30 , 0)];

    [self.cameraButton setImage:[UIImage imageNamed:@"Camera.png"] forState:UIControlStateNormal];
    [self.cameraButton.imageView setContentMode:UIViewContentModeScaleAspectFit];

    
    self.cameraButton.layer.borderColor = [rgb(255 , 255 , 255) CGColor];
    self.cameraButton.layer.borderWidth = 1;
    
    [self.view addSubview:self.cameraButton];
    [self.cameraButton addTarget:self action:@selector(onCameraButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    

    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setFrame:CGRectMake(size - 2 , -1 , size + 3 , size)];
    [self.closeButton setBackgroundColor:self.cameraButton.backgroundColor];
    [self.closeButton setTitleColor:rgb(255 , 255 , 255) forState:UIControlStateNormal];
    [self.closeButton setImageEdgeInsets:UIEdgeInsetsMake(30 , 0 , 30 , 0)];

    [self.closeButton setImage:[UIImage imageNamed:@"Close.png"] forState:UIControlStateNormal];
    [self.closeButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    self.closeButton.layer.borderColor = [rgb(255 , 255 , 255) CGColor];
    self.closeButton.layer.borderWidth = 1;
    
    [self.view addSubview:self.closeButton];
    [self.closeButton addTarget:self action:@selector(onCloseButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}


- (void)createBottomButtons
{
    CGFloat size = 0.5 * CGRectGetWidth(self.view.bounds);

    self.okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.okButton setFrame:CGRectMake(0 , 0 , size , size)];
    [self.okButton setBackgroundColor:rgb(255 , 255 , 255)];
    [self.okButton setTitleColor:rgb(0 , 255 , 0) forState:UIControlStateNormal];
    [self.okButton setImageEdgeInsets:UIEdgeInsetsMake(0 , 20 , 0 , 20)];

    [self.okButton setImage:[UIImage imageNamed:@"OK.png"] forState:UIControlStateNormal];
    [self.okButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.bottomContainer addSubview:self.okButton];
    [self.okButton addTarget:self action:@selector(onOKButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    self.helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.helpButton setFrame:CGRectMake(size + 1 , 0 , size - 1 , size)];
    [self.helpButton setBackgroundColor:rgb(255 , 255 , 255)];
    [self.helpButton setTitleColor:rgb(255 , 0 , 0) forState:UIControlStateNormal];
    [self.helpButton setImageEdgeInsets:UIEdgeInsetsMake(0 , 20 , 0 , 20)];

    [self.helpButton setImage:[UIImage imageNamed:@"Help.png"] forState:UIControlStateNormal];
    [self.helpButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.bottomContainer addSubview:self.helpButton];
    [self.helpButton addTarget:self action:@selector(onHelpButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)updateTimeLabel
{
    [self.timeTextLabel setText:[self.timeFormatter stringFromDate:[NSDate date]]];
}

- (void)updateNextAlarmTimeLabel
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmm"];
    
    NSString *nextAlarmTimeString = nil;
    NSInteger currentTimeInterval = [[formatter stringFromDate:[NSDate date]] integerValue];
    for (NSString * dateString in self.alarmTimeArray)
    {
        NSInteger date = [[dateString stringByReplacingOccurrencesOfString:@":" withString:@""] integerValue];
        if (date >  currentTimeInterval)
        {
            nextAlarmTimeString = dateString;
            [self.nextAlarmNoticeView setText:[NSString stringWithFormat:@"下次响铃：%@" , dateString]];
            break;
        }
    }
    
    if (nextAlarmTimeString == nil)
        [self.nextAlarmNoticeView setText:[NSString stringWithFormat:@"下次响铃：%@" , self.alarmTimeArray[0]]];
}


- (void)onCameraButtonPressed:(id)sender
{
    [self removeOKButtonBackgroundAnimation];
    [self stopRingTheBell];
 
    CGFloat topAnimateFactor = self.isCameraOn ? -100 : 100;
    CGFloat bottomAnimateFactor = self.isCameraOn ? -60 : 60;
    
    /* Animate the top buttons */
    CGRect cameraButtonFrame = self.cameraButton.frame;
    CGRect closeButtonFrame = self.closeButton.frame;
    
    cameraButtonFrame.size.height -= topAnimateFactor;
    closeButtonFrame.size.height -= topAnimateFactor;
    
    /* Animate bottom buttons */
    CGRect okButtonFrame = self.okButton.frame;
    CGRect helpButtonFrame = self.helpButton.frame;
    
    okButtonFrame.origin.y += bottomAnimateFactor;
    okButtonFrame.size.height -= bottomAnimateFactor;
    
    helpButtonFrame.origin.y += bottomAnimateFactor;
    helpButtonFrame.size.height -= bottomAnimateFactor;
    
    CGFloat top = self.isCameraOn ? 30 : 0;
    CGFloat bottom = self.isCameraOn ? 30 : 0;
    UIEdgeInsets topIconEdges = UIEdgeInsetsMake(top , 0 , bottom , 0);
    
    CGFloat left = self.isCameraOn ? 20 : 0;
    CGFloat right = self.isCameraOn ? 20 : 0;
    UIEdgeInsets bottomIconEdges = UIEdgeInsetsMake(0 , left , 0 , right);
    
    /* Animate preview view for video tapping */
    CGRect previewViewFrame = self.previewView.frame;
    previewViewFrame.origin.y = CGRectGetMaxY(cameraButtonFrame) + 1;
    previewViewFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetHeight(cameraButtonFrame) - CGRectGetHeight(okButtonFrame) - 1;
    
    CGFloat alpha = self.isCameraOn ? 0 : 1;
    if (!self.isCameraOn)
    {
        [self.view addSubview:self.previewView];
        [self.view bringSubviewToFront:self.previewView];
    }
    
    __weak MyViewController *__weakSelf = self;
    [UIView animateWithDuration:0.5 animations:^{
        [__weakSelf.cameraButton setFrame:cameraButtonFrame];
        [__weakSelf.cameraButton setImageEdgeInsets:topIconEdges];
        
        [__weakSelf.closeButton setFrame:closeButtonFrame];
        [__weakSelf.closeButton setImageEdgeInsets:topIconEdges];
        
        [__weakSelf.okButton setFrame:okButtonFrame];
        [__weakSelf.okButton setImageEdgeInsets:bottomIconEdges];
        
        [__weakSelf.helpButton setFrame:helpButtonFrame];
        [__weakSelf.helpButton setImageEdgeInsets:bottomIconEdges];
        
        [__weakSelf.previewView setFrame:previewViewFrame];
        [__weakSelf.previewView setAlpha:alpha];
    } completion:^(BOOL finished) {
        __weakSelf.isCameraOn = !__weakSelf.isCameraOn;
        
        /* Remove the preview view when camera is off */
        if (!__weakSelf.isCameraOn)
            [__weakSelf.previewView removeFromSuperview];
    
        [self setNeedsStatusBarAppearanceUpdate];
        [self startVideoRecording:self.isCameraOn];
    }];
}


- (void)onHelpButtonPressed:(id)sender
{
    [self removeOKButtonBackgroundAnimation];
    [self stopRingTheBell];

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"开启视频求助" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
    [alertView show];
    
    if (!self.isCameraOn)
        [self onCameraButtonPressed:nil];
    
    [self updateNextAlarmTimeLabel];

}

- (void)onOKButtonPressed
{
    [self removeOKButtonBackgroundAnimation];
    [self stopRingTheBell];

    NSString *message = nil;
    if (self.isAlarming)
    {
        message = @"报平安！";
        self.isAlarming = NO;
    }
    
    else
        message = @"尚未到闹铃时间，不可进行该操作";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
    [alertView show];
    
    [self updateNextAlarmTimeLabel];
    [self scheduleNextAlarm];
}


- (void)onCloseButtonPressed
{
    [self removeOKButtonBackgroundAnimation];
    [self stopRingTheBell];

    if (self.isAlarming)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您这个时间段尚未报平安，请报平安！" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
        [alertView show];
        
        return;
    }
    [self.updateTimeLabelTimer invalidate];
    self.updateTimeLabelTimer = nil;
    
    if ([self.delegate respondsToSelector:@selector(dismissMyViewController)])
        [self.delegate dismissMyViewController];
}


- (void)startVideoRecording:(BOOL)start
{
    if (start == YES)
    {
        self.session = [[AVCaptureSession alloc] init];
        self.session.sessionPreset = AVCaptureSessionPresetMedium;
        
        NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        
        AVCaptureDevice *deviceCamera = nil;
        
        for (AVCaptureDevice *device in devices)
        {
            if ([device position] == AVCaptureDevicePositionFront)
                deviceCamera = device;
        }
        
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:deviceCamera error:&error];
        if (!input)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"检测不到照相机" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles: nil];
            [alertView show];
        }
        [self.session addInput:input];
        
        AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
        [self.session addOutput:output];
        
        dispatch_queue_t queue = dispatch_queue_create("queue", nil);
        [output setSampleBufferDelegate:self queue:queue];
        
        output.videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithInt:kCVPixelFormatType_32BGRA], kCVPixelBufferPixelFormatTypeKey,
                                [NSNumber numberWithInt:320] , kCVPixelBufferWidthKey,
                                [NSNumber numberWithInt:240] , kCVPixelBufferHeightKey,
                                nil];
        
        self.sessionPreLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
        self.sessionPreLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        self.sessionPreLayer.frame = self.previewView.bounds;
        [self.previewView.layer addSublayer:self.sessionPreLayer];
        [self.session startRunning];
 
    }
    
    else
    {
        [self.session stopRunning];
        self.session = nil;
        [self.sessionPreLayer removeFromSuperlayer];
    }
}

#pragma mark - AVCaptureVideoDataOutputSampleBuffer Delegate Method
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    
}

@end
