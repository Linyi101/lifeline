//
//  BellRinger.m
//  YzHomeKit
//
//  Created by Sooyo on 14-7-17.
//  Copyright (c) 2014年 Sooyo. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>

#import "BellRinger.h"
//#import "Configuration.h"

/* Define a line that can make you understand the contorlflow easier */
#define Return_From_Thread return

extern NSString *const RingtoneFileSuffix;

static void  ringtoneCompleteProc(SystemSoundID soundID , void  *clientData)
{
    BOOL shouldStop = *((BOOL*)clientData);
    if(shouldStop)
        return;

    AudioServicesPlaySystemSound(soundID);
}


static void vibrateCompleteProc(SystemSoundID soundID , void *clientData)
{
    BOOL shouldStop  = *((BOOL*)clientData);
    if(shouldStop)
        return;
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


@interface BellRinger ()

@property (nonatomic) SystemSoundID soundID;

@property (nonatomic) BOOL shouldStop;
@property (nonatomic) BOOL running;
@property (nonatomic) BOOL shouldDeviceVibrate;
@property (nonatomic) BOOL isRingtoneEnable;
@property (nonatomic) CGFloat originRingtoneVolume;

@end

@implementation BellRinger

- (id)init
{
    self = [super init];
    if(self)
    {
        self.shouldStop = NO;
        self.running = NO;
    }
    return self;
}


- (void)dealloc
{
    AudioServicesDisposeSystemSoundID(self.soundID);
}


#pragma mark - Public Funcions
- (void)startToRingTheBell
{
    self.running = YES;
    
//    self.shouldDeviceVibrate = [Configuration shouldDeviceVibrate];
//    self.isRingtoneEnable = [Configuration isRingtoneEnable];
    
//    if(!self.shouldDeviceVibrate && !self.isRingtoneEnable)
//    {
//        [Configuration setDeviceVibrate:YES];
//        [Configuration setRingtoneEnable:YES];
//        
//        self.isRingtoneEnable = YES;
//        self.shouldDeviceVibrate = YES;
//    }
    
//    if(self.isRingtoneEnable)
    {
//        NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:[Configuration preferRingtone] withExtension:RingtoneFileSuffix];
        NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"Beepbeep" withExtension:@".aif"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)fileUrl , &_soundID);
        AudioServicesAddSystemSoundCompletion(self.soundID , NULL , NULL , ringtoneCompleteProc , &_shouldStop);
        AudioServicesPlaySystemSound(self.soundID);
    }
    
//    if(self.shouldDeviceVibrate)
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate , NULL , NULL , vibrateCompleteProc , &_shouldStop);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}


- (void)stopRingingTheBell
{
    self.shouldStop = YES;
    self.running = NO;
    
    AudioServicesDisposeSystemSoundID(self.soundID);
    AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate);
    
    if([self.delegate respondsToSelector:@selector(onBellDidStopRinging)])
        [self.delegate onBellDidStopRinging];
}

- (BOOL)isBellRinging
{
    return self.running;
}

@end
