//
//  MyViewController.h
//  TestLocalNotification
//
//  Created by Sooyo on 14-12-22.
//  Copyright (c) 2014年 Sooyo. All rights reserved.
//

#import <UIKit/UIKit.h>


/* Controller status */
typedef NS_ENUM(NSInteger , LifeLineStatus)
{
    LifeLineStatusResting = 0,
    LifeLineStatusRingingBell,
    LifeLineStatusPausingForNextRing,
};

@protocol  MyViewControllerDelegaete <NSObject>

@required
- (void)dismissMyViewController;

@end

@interface MyViewController : UIViewController

@property (nonatomic , weak) id<MyViewControllerDelegaete> delegate;
@property (nonatomic , assign) LifeLineStatus lifeLineStatus;


@end

