//
//  ViewController.m
//  TestLocalNotification
//
//  Created by Sooyo on 14-12-22.
//  Copyright (c) 2014年 Sooyo. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "MyViewController.h"

@interface ViewController () <MyViewControllerDelegaete>

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic , assign) BOOL needToRingTheBell;


- (IBAction)setAlarm:(id)sender;
- (void)presentLifeLineController;


@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.datePicker setTimeZone:[NSTimeZone defaultTimeZone]];
    [self.navigationController setNavigationBarHidden:YES];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentLifeLineController) name:KLifeLineQueryNotificationName object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (IBAction)setAlarm:(id)sender
{
    NSDate *date = self.datePicker.date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeStyle = NSDateFormatterShortStyle;
    formatter.dateStyle = NSDateFormatterShortStyle;
    
    NSString *dateString = [formatter stringFromDate:date];
    NSLog(@"DateString %@" , dateString);

    NSDate *triggerDate = [formatter dateFromString:dateString];
    NSLog(@"%@", triggerDate);
    
    /* Make local notification */
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = triggerDate;
    notification.alertBody = @"平安钟，请您报平安";
    notification.soundName = @"Beepbeep.aif";
    notification.userInfo = @{@"Type" : @"LifeLine"};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    [self performSegueWithIdentifier:@"segue" sender:self];

}

- (void)presentLifeLineController
{
    self.needToRingTheBell = YES;
    [self performSegueWithIdentifier:@"segue" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue"])
    {
        MyViewController *destin = (MyViewController*)segue.destinationViewController;
        [destin setDelegate:self];
        if (self.needToRingTheBell)
            [destin setLifeLineStatus:LifeLineStatusRingingBell];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void)dismissMyViewController
{
    NSLog(@"Disimie");
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentLifeLineController) name:KLifeLineQueryNotificationName object:nil];
    }];
}

@end
