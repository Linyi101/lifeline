//
//  AppDelegate.h
//  TestLocalNotification
//
//  Created by Sooyo on 14-12-22.
//  Copyright (c) 2014年 Sooyo. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const KLifeLineQueryNotificationName;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
