//
//  BellRinger.h
//  YzHomeKit
//
//  Created by Sooyo on 14-7-17.
//  Copyright (c) 2014年 Sooyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BellRinggerDelegate <NSObject>

@optional
- (void)onBellDidStopRinging;
@end

@interface BellRinger : NSObject

@property (nonatomic , weak) id<BellRinggerDelegate> delegate;

- (void)startToRingTheBell;
- (void)stopRingingTheBell;

- (BOOL)isBellRinging;

@end
